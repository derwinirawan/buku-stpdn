
# Bagaimana cara membangun jejaring?

> Oleh: Dasapta Erwin Irawan dan Miftachul Huda

Jejaring, seringkali begitu diagungkan. Seakan kiamat, jika kita tak punya jejaring. Masa iya sih? Dalam hal riset, jejaring (kolaborator) bisa diperlukan, bisa jadi tidak.
  
Jika saya amati publikasi pembimbing, tampak ada kemiripan satu sama lain. Mereka cenderung _single fighter_. Selaras dengan kalimat, “Jika Anda berani, bertarunglah sendirian”.  
  
Pun, jika kolaborasi paling berdua atau bertiga. Karya-karya yang dianggap fenomenal, justru lahir saat _single author._

## Catatan pembaca

-----
-----
-----
<!--stackedit_data:
eyJoaXN0b3J5IjpbLTQwMzgwOTMwNCw3ODU3NjQ3NjEsLTIxND
Y0NDEyMDgsMTY5NzEyODYwMiwtMTk0ODcyMTU2NF19
-->