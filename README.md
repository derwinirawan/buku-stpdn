# buku-stpdn

STPDN adalah akronim dari Buku Sukses sTudi Pascasarjana di Dalam Negeri. Buku ini akan dipublikasikan sebagai buku akses terbuka (*open access*) di [Leanpub](leanpub.com) dan edisi cetaknya akan diterbitkan oleh ITBPress.

# Penulis

## Dasapta Erwin Irawan

Nama saya Erwin, dosen di Prodi S1 Teknik Geologi dan Prodi S2 Teknik Air Tanah Fakultas Ilmu dan Teknologi Kebumian, Institut Teknologi Bandung. Fokus riset saya adalah hidrogeologi dan hidrokimia. Sehari-hari saya mengurus keluarga, mengajar, meneliti, dan mempromosikan penggunakan perangkat lunak kode terbuka (R, Python) dan mendemokratisasikan pengetahuan melalui gerakan sains terbuka. Alamat surel saya: [dasaptaerwin3@gmail.com](mailto:dasaptaerwin3@gmail.com)

## Astadi Pangarso

Halo nama saya adalah Astadi Pangarso, saya seorang kandidat doktor ilmu Administrasi dari Universitas Brawijaya Malang. Saya juga merupakan awardee BUDI DN 2016, BUDI DN merupakan singkatan dari Beasiswa Untuk Dosen Indonesia Dalam Negeri. Saya juga seorang dosen di prodi Administrasi Bisnis, Universitas Telkom Bandung. Terkait dengan studi dengan skema beasiswa yang saya dapat mengharuskan saya untuk tugas belajar selama kontrak 36 bulan+12 bulan. Kali ini saya ingin membagikan beberapa hal yang terkait sukses studi pasca sarjana di dalam negeri. Harapan saya tulisan ini dapat menolong bagi rekan-rekan yang akan atau sedang menyelesaikan studi pasca sarjananya. Selamat dan sukses studi pasca sarjana hingga lulus tepat waktu dengan proses dan hasil terbaik.
Alamat surel: [astadipangarso@telkomuniversity.ac.id](mailto:astadipangarso@telkomuniversity.ac.id) dan [astadipangarso@student.ub.ac.id](mailto:astadipangarso@student.ub.ac.id)

## Mifhtachul Hadi

---
---
---



<!--stackedit_data:
eyJoaXN0b3J5IjpbLTg0MjA5NTAwNiwtNjMzNjM3MTcyXX0=
-->