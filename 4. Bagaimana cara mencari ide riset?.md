
# Bagaimana cara mencari ide riset?

> Oleh: Dasapta Erwin Irawan dan Astadi Pangarso

Seluruh program pascasarjana di Indonesia, S2 (magister) atau S3 (doktor), adalah program riset. Kelulusan anda akan ditentukan oleh riset anda dan bagaimana anda dapat mempertahankannya. Oleh karenanya wajar kalau saya bilang bahwa riset anda bukan dimulai pada semester 4 untuk program S2 atau semester 6 untuk program S3. Riset anda dimulai sejak anda masuk, bahkan sejak anda belum mulai.

Kok bisa dimulai sejak belum masuk?

Saya balik bertanya, bukankah anda diminta membuat proposal riset saat pendaftaran dan anda telah menghabiskan banyak waktu untuk menyusunnya. Yang banyak terjadi adalah anda melupakannya saat anda masuk. Waktu bergulir, hingga suatu saat anda sadar harus lulus dalam waktu dekat.

Berikut ini adalah rencana yang sering saya beri nama **Rencana B** (*Plan B*), yaitu sebuah rencana yang dapat anda laksanakan tanpa bantuan orang lain, termasuk pembimbing, khususnya dalam hal finansial atau fasilitas pendukung. 

Jadi hal pertama yang anda butuhkan saat mencari ide riset, adalah **konsistensi**.

## Konsistensi

Saya tidak melarang ada perubahan, tetapi pikiran kita ini punya pola. Tapi perubahan yang tidak terencana tidak kita inginkan saat mengikuti program pascasarjana. Apa yang sudah dipikirkan sejak awal, biasanya adalah pilihan yang dapat meluluskan anda. Mungkin anda tidak sadar dan lebih memilih alternatif lain yang muncul di tengah jalan. 

Misal: saat anda berencana masuk program pascasarjana, anda membuat proposal tentang pemetaan kualitas air. Proposalnya sudah anda buat, lengkap dengan latar belakang, tinjauan pustaka, metode, anggaran biaya hingga luaran yang diharapkan.

## Rasional

Selain konsistensi, anda juga perlu **rasional** atas pilihan anda. Rasional bisa didasarkan kepada beberapa komponen:

- **dana**: ini jelas. Apalagi kalau dana riset berasal dari kantong anda atau orangtua anda. Jadi jangan merencanakan riset yang tidak mungkin anda biayai, misal: memilih daerah riset di lokasi yang jauh.
- **waktu**: ini juga penting. Program S2 maupun S3 pasti ada waktu maksimumnya. Anda harus mampu merencanakan riset yang dapat diselesaikan dalam waktu selambatnya empat semester untuk S2, dan enam semester untuk S3, misal: janganlah memilih riset yang datanya perlu waktu lama karena daerahnya terlalu luas.
- **perangkat**: riset harus dapat diselesaikan dengan perangkat yang anda punya, misal laptop dengan spesifikasi i5 atau i7. Kalau anda hanya punya laptop i3 dan tidak punya fasilitas yang ditawarkan pembimbing (misal PC di laboratoriu), maka jangan memilih riset yang hanya dapat diselesaikan dengan *super computer*.
- **keterampilan**: idealnya anda lulus S2 atau S3 membawa keterampilan baru. Itu benar. Tapi kalau keterampilan itu malah memberatkan anda, dalam arti, berdampak memperlambat studi secara sifnifikan, maka bisa saja dihapus dulu. Misal: anda belum pernah membuat program komputer, padahal topik riset anda bertumpu kepada keterampilan itu. Memang bisa dipelajari, tapi akan butuh ketekunan dan ekstra waktu di luar jam kerja riset yang biasa anda habiskan. Kalau anda bisa melakukan itu, maka bagus. Saya juga senang, karena salah satu yang akan menjadi pengingat anda setelah lulus adalah keterampilan baru. Kalau tidak, anda mungkin perlu mencari alternatif, misal bahasa pemrograman yang lebih mudah dibanding yang diajukan pembimbing. 

## Manfaatkan jejaring dan teknologi

Mencari ide kadang-kadang yang anda perlukan adalah kawan. Oleh karenanya, saran saya adalah manfaatkan jejaring dan teknologi.

**manfaatkan jejaring**:
- cari teman banyak: anda lebih tahu bagaimana caranya,
- bagikan ide anda: jangan ragu-ragu saat akan menjelaskan ide anda. Kalau idenya sudah matang yang tidak perlu banyak tanya kan.,
- minta masukan: jangan malu untuk minta masukan dari banyak orang (yang bekerja di bidang anda),
- atau ikut seminar: lakukan bila ada biaya, seminar nasional saja,
- atau buat seminar: jangan lupa beli penganan ringan sebagai penarik minat.

**manfaatkan teknologi**:
- tulis proposal, 
- save online,
- publikasikan melalui kanal medsos (dianjurkan twitter),
- presentasikan via youtube (misalnya),
- gabung dengan beberapa GWA yang beranggotakan mahasiswa, atau buat grup sendiri,
- aktifkan layanan notifikasi publikasi, misal *Google Scholar Alert*,
- buat analisis bibliometrik tentang topik yang relevan ([contoh referensi](https://riojournal.com/article/9841/)). 

Tapi ingat kalau anda ingin dapat masukan, maka jangan malas memberikan masukan kepada anggota yang lain. Bermurah hatilah untuk menyebarkan apa yang anda tahu. Apalagi kalau anda anggota baru dalam suatu forum.

> Astadi Pangarso

Untuk mencari ide riset maka saya akan memberikan beberapa pedoman menurut pengalaman pribadi saya antara lain:

1. Apa penelitian terakhir yang dilakukan? Dari sini didapat kira-kira dapat diteliti yang sesuai dengan penelitian terakhir yang dilakukan untuk diperdalam.
2. Topik apa yang sesuai dengan keilmuan studi pasca sarjana yang sesuai dengan minat, sedang menjadi isu penting baik secara nasional maupun internasional secara praktis? Dari sini biasanya bisa dilanjutkan dengan menghubungkan permasalahan praktis tersebut dengan solusi secara keilmuan yang ada pada artikel jurnal ilmiah terkemuka dan terbaru.
3. Konsultasi dengan tim pembimbing. Biasanya beliau akan memberi gambaran ide riset yang baik dan realistis untuk diselesaikan secara waktu dalam rentang waktu studi.

Terhubung dengan kolega rekan-rekan sekeilmuan. Hal ini biasanya didapat melalui social media antara lain: [Research Gate](https://www.researchgate.net/), [Academia.edu](Academia.edu), grup-grup di [Facebook](facebook.com), [Linked In](linkedin.com) serta grup-grup whatsapp serta telegram baik nasional maupun internasional. Di media tersebut kita dapat bertanya misal seperti ini: `Halo nama saya Astadi saya sekarang merupakan mahasiswa doktoral ilmu administrasi bisnis. Apa kira-kira ide riset yang cukup menarik diteliti dengan keilmuan saya? Mohon sumbang saran. Terima kasih`.

> Miftachul Huda

Ide memang tak bisa kita paksa untuk menghampiri kita. Waktunya pun mau-mau dia. Namun, tampaknya kedatangan ide bisa kita kondisikan. Biasanya, ia datang di saat kondisi badan kita lagi segar. Saat mandi pagi, misalnya.  
Tentunya, sebelum fase kedatangan ide, kita mesti cukup memasukkan bahan untuk diolah pikiran. Membaca atau memikirkannya sebelum tidur bisa kita coba.

## Catatan pembaca

-----
-----
-----
<!--stackedit_data:
eyJoaXN0b3J5IjpbMTg5MzMyMDA3OSwyMDI3MTA4MDc1LDE4OT
MzMjAwNzksLTE4NTgyMDQ0NSwtMjE0MzE4NTM3NCwtMTc3MjQy
MzA0NiwxMzE3OTQxOTk5LC0zOTM4NDg4NV19
-->