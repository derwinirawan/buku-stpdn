
# Apakah harus mengikuti pendidikan pascasarjana?

> Oleh: Dasapta Erwin Irawan dan Astadi Pangarso

Jawaban singkatnya adalah **tidak**. Keputusan akan bergantung kepada pilihan karir. Yang sudah pasti memerlukan pendidikan tingkat lanjut adalah kalau memilih untuk menjadi dosen/peneliti (kelompok akademia). Jawaban cepat ini tentunya tidak menutup kesempatan bagi yang tidak ingin menjadi akademia tetapi ingin mengikuti pendidikan pascasarjana. Tentunya bebas saja. Setidaknya kalau pilihan profesinya non akademia, maka tidak akan terlalu terkait dengan **isu linearitas**.

![pilihan karir](https://lh3.googleusercontent.com/qimONbH6vJy7aTK8Hg0rIlnE5Ea1TY11fJz6TqwNLzMgaI1JpwJX7d4MLR5TSQCoQsxNZotvQr8gknwEbptXP8HWtMNHbPwU1tb7aQrpK3XnuPitQA8SjcInKVZf24SKoiM_L8uJIHTIAqC91x8pz_g_bYMVzrPk3z1fpOzTPS-e2o-UuDQlVqTe3-mhCEPs4F3totmKQ8LYQZc_QAFHIAsPBujRECueOvhSS8YZcuN3Bpi7tQJF88FmRxTMxLA6HEDBaeMnTd-EqRiF8pStX8tKPttG2IHc1-x4aPNXPsdk-K0dmlEAkE87_Xm-rd8AZ5dV038rPwcdS9GU39IxeoiHAqPODLJR9rsCPLyeSc5kfrjYo57xBPiJEO8d86VibXzlh0LxFdhTHIzGRD8ztfAg1af_C08mQs-K8HQrTkSsWRSO0bgdOCnEE7WFGkhcTo4tUxFVH3t6ShWuArXASgztwq3sanIyQnszd2bMevDxBHcxlL19fygHvk5_TLjwkQPvolo2qZrXxPQLe0lQb3EytCSuEN11wYs5xLc6ZB2zMZR0FK4vt6ILcq8jNUrK-58E3_AU2j1DR1guN8rHzlnB3_FGhEqQ1EhLg_1IYbL6_BDRjLRF1E15KJfZoR9RPNxJUbnufbIo7_rzmnPqtGV-ha600oLk15xdIxgNy02tXJE0vr7Lhw=w1988-h1364-no)

## Memilih untuk menjadi non-akademia (jalur kiri)
 
Seperti telah disampaikan, bahwa pilihan karir ini tidak wajib untuk mengikuti studi pascasarjana. Namun demikian, makin ke sini, makin banyak lembaga formal (lembaga pemerintahan, industri, perusahaan atau kantor secara umum) yang meminta pegawainya untuk melanjutkan pendidikan ke jenjang lebih tinggi. Salah satu alasan kuatnya sebenarnya administratif, yakni untuk kebutuhan promosi jabatan. Jenjang S2 setidaknya dibutuhkan dalam kasus ini. 

**Isu linearitas** yang sering muncul untuk kelompok akademia biasanya tidak muncul. Yang linear di sini adalah keserumpunan bidang ilmu dengan jenjang pendidikan sebelumnya, yang kemudian diterjemahkan terlalu sempit sebagai nama program studi yang sama. Dalam kasus ini linearitas ini lebih berkaitan dengan relevansi program pendidikan dengan tugas sehari-hari yang harus diemban di perusahaan, misal:

- untuk yang bekerja mengelola sumber daya manusia (SDM) biasanya akan mengambil program magister SDM juga. Tapi ada juga yang mengambil program yang berhubungan dengan sistem informasi,
- bagi para pengelola keuangan, maka biasanya akan mengikuti pendidikan magister manajemen atau akuntansi,
- dst.

Bagi beberapa orang pilihan karir sebagai non-akademia adalah yang terbaik, karena biasanya indikator kinerjanya jelas. Pegawai dinilai berdasarkan hasil kerjanya secara riil dan biasanya kriterianya dikembangkan sendiri oleh perusahaan atau lembaga. Walaupun pengukuran kinerjanya tidak rumit, tapi pola kerjanya tidak bisa dibilang sederhana. 

## Memilih untuk menjadi akademia (jalur kanan)

Banyak yang bilang jalur ini relatif mudah dengan pengelolaan waktu yang fleksibel. Tidak sepenuhnya salah, tapi makin ke sini, kegiatan yang harus dilakukan bertambah. Sejak dahulu akademia memiliki **tiga tugas utama Tridarma Perguruan Tinggi**, yaitu **pendidikan, penelitian, dan pengabdian kepada masyarakat**. 

Sampai di sini banyak yang masih bisa bertahan. 

Tapi kemudian kondisi yang tidak kondusif adalah banyaknya indikator kinerja yang harus dipenuhi, saat melakukan Tridarma Perguruan Tinggi itu. Masalah utamanya adalah indikator kinerja itu banyak yang ditentukan oleh pihak lain, misal: penerbit makalah, lembaga pengindeks, lembaga pemeringkatan dll. Masalah berikutnya adalah negara melalui Kementerian Riset, Teknologi, dan Pendidikan Tinggi (Kemenristekdikti) menyeragamkan cara mengukur kinerja. Jadi **ada banyak tangan yang terlibat dapat pengukuran kinerja**, tidak seperti jalur yang satu lagi di atas. 

Belum lagi kalau bicara **linearitas** yang diartikan sempit. Anda yang memilih jalur akademia harus memperhatikan ini. Salah pilih jurusan studi, bisa malah menghambat karir anda.

## Tapi apakah mengambil jalur kanan begitu buruk?

Jawabnya juga **tidak**. Memang saya sedang mempermainkan logika anda.

Mengambil jalur kanan berarti anda juga harus bersiap mengenal banyak orang dan masuk ke banyak lingkungan berbeda. 

Apakah akan selalu begitu?

**Tidak juga**. 

Tergantung dari anda juga. Makin anda ingin banyak belajar, maka kesempatan akan terbuka. Bukan sebaliknya. Karena semakin anda merasa pintar, maka justru tertutup kesempatannya. 

> Astadi Pangarso

Jawaban saya juga “tidak”. Hal ini karena akan tergantung dengan alasan mengapa kita memutuskan untuk studi pasca sarjana. Jika dikaitkan dengan saya maka karena saya seorang dosen maka hal tersebut merupakan keharusan, khususnya juga karena tempat saya bekerja sangat mendorong agar dosen-dosennya berkualifikasi doktor. Dengan kualifikasi doktor maka seorang dosen berkesempatan untuk dapat belajar banyak hal secara lebih spesifik sehingga dari sana diharapkan akan banyak dampak yang baik bagik bagi pribadinya maupun bagi institusi tempat dimana dosen tersebut bekerja dan mengabdi.

Kualifikasi doktor merupakan kualifikasi pendidikan formal tertinggi yang secara spefisik mempelajari suatu bidang studi tertentu. Jenjang doktor merupakan langkah awal seseorang dapat belajar tentang meneliti sesuatu dengan spesifik melalui proses yang cukup panjang. Jika dikaitkan dengan konteks studi dalam negeri jika boleh saya membagikan pengalaman studi saya maka untuk seseorang dapat memperoleh gelar doktor harus melalui kurang lebih sembilan proses atau tahapan khusus terkait dengan penelitian disertasinya. Didalam kesembilan tahapan tersebut terdapat proses bimbingan bersama tim promotor/pembimbing dan juga proses pengambilan data penelitian. Biasanya kesembilan tahapan ini rata-rata dapat diselesaikan seorang kandidat doktor dalam waktu antara 3-4 tahun.

Untuk memutuskan apakah seseorang melanjutkan studinya minimal ada beberapa hal yang menjadi pertimbangan:

1. Apakah pentingnya studi bagi yang bersangkutan terkait dengan dampak baik yang didapat setelah menyelesaikan studi?
2. Apakah yang bersangkutan memiliki sumber daya (waktu, tenaga, dana) yang cukup terkait studi pasca sarjananya?

> Miftachul Huda

Untuk bisa melakukan riset, biasanya kita dituntut untuk selesai studi master (S2) dan doktoral (S3).  Namun, hal ini tentunya tidak mutlak. Sejarah fisika di tanah air, sebagai contoh, ada nama Hans Jacobus Wospakrik. Paper beliau: _Classical Equation of Motion of A Spinning Nonabelian Test Body In General Relativity_, tembus Physical Review D di tahun 1982 berbekal studi S1 dan studi mandiri.

## Catatan pembaca

-----
-----
-----
<!--stackedit_data:
eyJoaXN0b3J5IjpbNTc0MzU5NDY2LDExNjUwNTg5MDEsLTIwOT
U1NDQ5MTksNTE0MTE1NTk5LC00NjQ5MTE5MzldfQ==
-->